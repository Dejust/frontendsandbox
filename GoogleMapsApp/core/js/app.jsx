var Map = require('../../blocks/map/map.jsx'),
	React = require('react'),
    ReactDOM = require('react-dom'),
    Page = require('../../blocks/page/page.jsx'),
    AppStore = require('./flux/app-store.js'),
    Promise = require('es6-promise-polyfill/promise.min.js').Promise;
    
if (!window.Promise) {
	window.Promise = Promise;	// ie polyfill
}

window.onload = function () {

	ReactDOM.render(
	  <Page />,
	  document.getElementsByClassName('page')[0]
	);
};