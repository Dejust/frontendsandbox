var	AppDispatcher = require('./app-dispatcher.js'),
	ActionTypes = require('./action-types.js'),
	Actions = {
		createPoint: function(coordinates) {
			AppDispatcher.dispatch({
				type: ActionTypes.CREATE_POINT,
				coordinates: coordinates
			});
		},

		deletePoint: function(id) {
			AppDispatcher.dispatch({
				type: ActionTypes.DELETE_POINT,
				id: id
			});
		},

		createObject: function(pointId, text) {
			AppDispatcher.dispatch({
				type: ActionTypes.CREATE_OBJECT,
				pointId: pointId,
				text: text
			});
		},

		deleteObject: function(pointId, objectId) {
			AppDispatcher.dispatch({
				type: ActionTypes.DELETE_OBJECT,
				pointId: pointId,
				objectId: objectId
			});
		},

		editPointText: function(id, text) {
			AppDispatcher.dispatch({
				type: ActionTypes.EDIT_POINT_TEXT,
				id: id,
				text: text
			});
		},

		editObjectText: function(pointId, objectId, text) {
			AppDispatcher.dispatch({
				type: ActionTypes.EDIT_OBJECT_TEXT,
				pointId: pointId,
				objectId: objectId,
				text: text
			});
		},

		selectPoint: function(id) {
			AppDispatcher.dispatch({
				type: ActionTypes.SELECT_POINT,
				id: id
			});
		}
	};

module.exports = Actions;