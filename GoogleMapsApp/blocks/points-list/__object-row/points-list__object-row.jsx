var React = require('react'),
	ReactDOM = require('react-dom'),
	BEMHelper = require('react-bem-helper'),
	DeleteButtonSmall = require('../../delete-button-small/delete-button-small.jsx'),
	EditButtonSmall = require('../../edit-button-small/edit-button-small.jsx'),
	AppStore = require('../../../core/js/flux/app-store.js'),
	Actions = require('../../../core/js/flux/actions.js');

var ObjectRow = React.createClass({
	bemHelper: new BEMHelper('points-list'),

	getDefaultProps: function() {
		return {
			maxLength: 100
		};
	},

	getStoreText: function() {
		var object = AppStore.getObject(this.props.pointId, this.props.id);
		if (object) {
			return object.text;
		}
		return false;			// no such object in store
	},

	getInitialState: function() {
		return {
			text: this.getStoreText(),
			disabled: true
		};
	},

	onChange: function() {
		this.setState({
			text: this.getStoreText()
		});
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this.onChange);
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this.onChange);
	},

	componentDidUpdate: function() {			// working with DOM only after updating component
		var nodeInput = ReactDOM.findDOMNode(this.refs.input);
		if (this.state.disabled) {
			nodeInput.blur();
		}
		else {
			nodeInput.focus();
			var length = this.state.text.length;
			nodeInput.setSelectionRange(length, length);	// set cursor at the end
		}
	},

	enable: function() {
		this.setState({disabled: false});
	},

	disable: function() {
		this.setState({disabled: true});
	},

	onBlur: function() {
		this.disable();
	},

	onKeyDown: function(event) {
		if (event.which === 13) {		// Return code
			this.disable();
		}
	},

	onInputChange: function(event) {
		Actions.editObjectText(this.props.pointId, this.props.id, event.target.value);
	},

	editHandler: function() {
		this.enable();
	},

	deleteHandler: function() {
		Actions.deleteObject(this.props.pointId, this.props.id);
	},

	render: function() {
		var classes = this.bemHelper;
		return (
			<li {...classes('object-row')}>
				<input
					value={this.state.text}
					ref={'input'}
					onBlur={this.onBlur}
					onKeyDown={this.onKeyDown}
					onChange={this.onInputChange}
					maxLength={this.props.maxLength}/>
				<EditButtonSmall onClick={this.editHandler}/>
				<DeleteButtonSmall onClick={this.deleteHandler}/>
			</li>
		);
	}
});

module.exports = ObjectRow;