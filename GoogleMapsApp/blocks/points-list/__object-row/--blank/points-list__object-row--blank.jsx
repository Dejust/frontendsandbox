var React = require('react'),
	ReactDOM = require('react-dom'),
	PropTypes = React.PropTypes,
	BEMHelper = require('react-bem-helper'),
	Actions = require('../../../../core/js/flux/actions.js');
	//AppStore = require('../../../../core/js/flux/app-store.js');

var ObjectRowBlank = React.createClass({
	bemHelper: new BEMHelper('points-list'),

	propTypes: {
		placeholder: PropTypes.string
	},

	getDefaultProps: function() {
		return {
			placeholder: 'новый объект'
		};
	},

	getInitialState: function() {
		return {
			focused: false
		};
	},

	onClick: function() {
		this.setState({focused: true});
	},

	onBlur: function() {
		this.setState({focused: false});
	},

	submit: function(text) {
		if (text.trim() !== '') {
			Actions.createObject(this.props.id, text.trim());
		}
	},

	onKeyDown: function(event) {
		if (event.which === 13) {				// Return code
			this.setState({focused: false});
			this.submit(event.target.value);
		}
	},

	onChange: function(event) {
		this.setState({text: event.target.value});
	},

	componentDidUpdate: function() {
		var nodeInput = ReactDOM.findDOMNode(this.refs.input);
		if (this.state.focused) {
			nodeInput.focus();
			return;
		}
		nodeInput.blur();
		nodeInput.value = '';					// show placeholder after blur
	},

	render: function() {
		var classes = this.bemHelper;
		return (
			<li
				{...classes('object-row', 'blank_true')}
				onClick={this.onClick}
				onBlur={this.onBlur}
				onKeyDown={this.onKeyDown}>
					<input
						placeholder={this.props.placeholder}
						ref={'input'}/>
			</li>
		);
	}
});

module.exports = ObjectRowBlank;