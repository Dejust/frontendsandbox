var React = require('react'),
	BEMHelper = require('react-bem-helper'),
	ObjectRow = require('../__object-row/points-list__object-row.jsx'),
	ObjectRowBlank = require('../__object-row/--blank/points-list__object-row--blank.jsx'),
	AppStore = require('../../../core/js/flux/app-store.js');

var ObjectsList = React.createClass({
	bemHelper: new BEMHelper('points-list'),

	getInitialState: function () {
		return {
			objects: AppStore.getObjects(this.props.id)
		};
	},

	onChange: function() {
		this.setState({objects: AppStore.getObjects(this.props.id)});
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this.onChange);
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this.onChange);
	},

	renderObjectRow: function (object) {
		return (
			<ObjectRow
				id={object.id}
				pointId={this.props.id}
				key={object.id}/>
		);
	},

	renderObjectRows: function() {
		var renderedObjectRows = [];
		for (var id in this.state.objects) {
			if (this.state.objects.hasOwnProperty(id)) {
				var renderedObjectRow = this.renderObjectRow(this.state.objects[id]);
				renderedObjectRows.push(renderedObjectRow);
			}
		}
		return renderedObjectRows;
	},

	render: function() {
		var classes = this.bemHelper;
		return (
			<ul {...classes('objects-list')}>
				{this.renderObjectRows()}
				<ObjectRowBlank
					id={this.props.id}/>
			</ul>
		);
	}
});

module.exports = ObjectsList;