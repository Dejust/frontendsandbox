var React = require('react'),
	BEMHelper = require('react-bem-helper');

var DeleteButton = React.createClass({
	bemHelper: new BEMHelper('delete-button'),

	render: function() {
		var classes = this.bemHelper;
		return (
			<button onClick={this.props.onClick} {...classes()}/>
		);
	}
});

module.exports = DeleteButton;