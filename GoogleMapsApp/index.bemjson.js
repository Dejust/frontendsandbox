module.exports.bemjson =
{
    tag: 'html',
    content:
    [
        {
            tag: 'head',
            content:
            [
                {
                    tag: 'meta',
                    attrs:
                    {
                        charset: 'utf-8'
                    }
                },
                {
                    tag: 'title',
                    content: 'Google Maps App'
                },
                {
                    tag: 'link',
                    attrs:
                    {
                        rel: 'icon',
                        href: 'images/icons/favicon.ico'
                    }
                },
                {
                    tag: 'script',
                    attrs:
                    {
                        src: 'http://maps.googleapis.com/maps/api/js'
                    }
                },
                {
                    tag: 'script',
                    attrs:
                    {
                        src: 'js/google-maps-app.js'
                    }
                },
                {
                    tag: 'link',
                    attrs:
                    {
                        rel: 'stylesheet',
                        type: 'text/css',
                        href: 'css/google-maps-app.css'
                    }
                },
            ]
        },
        {
            tag: 'body',
            content: {
                block: 'page'
            }
        }
    ],
};